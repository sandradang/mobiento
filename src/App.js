import React, { Component } from 'react';
import axios from 'axios';

import './App.css';

import SearchForm from './components/SearchForm.js';
import ImageList from './components/ImageList';

const ACCESS_KEY = 'ad99638efdcc7d84118bc607f09b56fd3ee109b4a01266e61d31d78786a4bc37';
const API_URL = 'https://api.unsplash.com/search/photos';

class App extends Component {
  constructor() {
    super();

    this.state = {
      keyword: '',
      images: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.searchImage = this.searchImage.bind(this);
  }

  componentDidMount() {
   this.searchImage('cat');
  }

  handleSubmit(event) {
    const { keyword } = this.state;

    event.preventDefault();
    this.searchImage(keyword);

  }

  searchImage(keyword) {
    axios.get(API_URL, {
      headers: {
        Authorization: `Client-ID ${ACCESS_KEY}`
      },
      params: { query: keyword, per_page: 50 }
    }).then(json => {
        this.setState({ images: json.data.results })
      })
  }

  handleChange(event) {
    const { value, name } = event.target;

    this.setState({
      [name]: value
    });
  }

  render() {
    const { keyword, images } = this.state;

    return (
      <div className="App">
        <SearchForm handleSubmit={this.handleSubmit} handleChange={this.handleChange} value={keyword} />
        <ImageList images={images} />
      </div>
    );
  }
}

export default App;
