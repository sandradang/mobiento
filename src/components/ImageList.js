import React from 'react';
import PropTypes from 'prop-types';
import ImageItem from './ImageItem';

const ImageList = ({ images }) => {
  
  let imageList = images.map((image) => (
      <ImageItem key={image.id} image={image} />
    )
  );

  return (
    <div className="image-container">
      {imageList}
    </div>
  )
};

export default ImageList;

ImageItem.propTypes = {
  images: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  )
};