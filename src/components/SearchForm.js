import React from 'react';
import PropTypes from 'prop-types';

const SearchForm = ({ handleSubmit, handleChange, value }) => {
    return (
      <div className="search-container">
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            name="keyword"
            value={value}
            onChange={handleChange}
            placeholder="Search images"
          />
          <input type="submit" value="Search"/>
        </form>
      </div>
    )
};

export default SearchForm;

SearchForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};