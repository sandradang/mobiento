import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ImageItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    };

    this.image = React.createRef();

    this.handleImageLoaded = this.handleImageLoaded.bind(this);
  }

  handleImageLoaded() {
    this.setState({ loaded: true })
  }

  render() {
    const { image } = this.props;
    let imageStyle;

    if (this.state.loaded) {
      imageStyle = {
        gridRowEnd: `span ${Math.ceil(this.image.current.height / 10)}`
      }
    }

    return (
      <div className="image-item" style={imageStyle}>
        <img
          src={image.urls.regular}
          alt={image.alt_description}
          ref={this.image}
          onLoad={this.handleImageLoaded}
        />
      </div>
    )
  }
}

export default ImageItem;

ImageItem.propTypes = {
  image: PropTypes.shape({
    urls: PropTypes.shape({
      regular: PropTypes.string.isRequired
    }).isRequired,
    alt_description: PropTypes.string.isRequired
  }).isRequired
};

